from dashboard.Link import BaseLink, UserLink

class BlogEditorLink(BaseLink):
    display = 'Blog'
    url = '/dashboard/blog'

class ProfileLink(UserLink):
    display = 'Profile'
    url = '/dashboard/profile'