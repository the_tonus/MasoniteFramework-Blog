from masonite.provider import ServiceProvider
from .links import BlogEditorLink, ProfileLink

from mistune import Markdown

class BlogProvider(ServiceProvider):
    
    def register(self):
        self.app.bind('BlogEditor', BlogEditorLink)
        self.app.bind('Profile', ProfileLink)

    def boot(self):
        pass

class RenderEngineProvider(ServiceProvider):

	wsgi = False

	def register(self):
		''' Registers the Html Render Engine Into The Service Container '''
		self.app.bind('RenderEngine', Markdown())

	def boot(self):
		pass

class RouteCompileProvider(ServiceProvider):
    ''' Adds Custom Route Compilers  '''

    wsgi = False


    def boot(self, Route):
        ''' Design custom route parameters '''
        Route.compile('slug', r'[a-zA-Z-]')
        Route.compile('author', r'[a-zA-Z]')
        Route.compile('category', r'[a-zA-Z]')