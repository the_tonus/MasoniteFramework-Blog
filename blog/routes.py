    
from masonite.routes import Get, Post, RouteGroup

def routes(prefix='/dashboard'):
     return RouteGroup([
        Get().route('/blog', 'BlogEditorController@show_all'),
        Get().route('/profile', 'ProfileController@show'),
        Post().route('/profile', 'ProfileController@store'),

        # Blog Editor
        RouteGroup([
            Get().route('/create', 'BlogEditorController@show_create'),
            Post().route('/create', 'BlogEditorController@create'),

            Get().route('/@slug/update', 'BlogEditorController@show_update'),
            Post().route('/@slug/update', 'BlogEditorController@update'),

            Get().route('/@slug/delete', 'BlogEditorController@show_delete'),
            Post().route('/@slug/delete', 'BlogEditorController@delete'),

            Get().route('/@slug/activate', 'BlogEditorController@activate'),
            Get().route('/@slug/deactivate', 'BlogEditorController@deactivate'),

            Get().route('/preview/@slug', 'BlogEditorController@preview')
        ], prefix="/post")

    ], prefix='/dashboard', middleware=('auth',))


