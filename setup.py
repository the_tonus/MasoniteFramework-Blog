from setuptools import setup

setup(
    name='masonite-blog',
    packages=[
        'blog',
        'blog.controllers',
        'blog.middleware',
        'blog',
    ],
    version='0.1',
    install_requires=[
        'masonite-dashboard',
        'unidecode'
    ],
    description='Masonite Blog',
    author='Tony Hammack',
    author_email='hammack.tony@gmail.com',
    url='https://github.com/hammacktony/MasoniteFramework-Blog',
    keywords=['python web framework', 'python3'],
    classifiers=[],
    include_package_data=True,
)
